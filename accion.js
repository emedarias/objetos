const estados = ["en adopción", "en proceso de adopción", "adoptado"]
const perrosRegistrados=[];

function Perros(nombre,estado)
	{
		this.nombre=nombre;
		this.estado=estado;
	}

Perros.prototype.modificar= function(opcion)
	{
		this.estado=estados[opcion];
		return this.estado;
	};

Perros.prototype.informar= function()
	{
		return this.nombre+" esta "+this.estado+".";
	};


function CargarPerro()
	{
		const nombre = prompt("Ingrese el nombre del perro.")
		const opcion = parseInt(prompt("Ingrese una opcion: 1 - en adopción, 2 - en proceso de adopción o 3 - adoptado"))-1;
		const perro = new Perros(nombre,estados[opcion]);
		perrosRegistrados.push(perro);
		Pregunta();		
	}

function Pregunta()
	{
		const respuesta = window.confirm("Deseas cargar otro perro?");
		if (respuesta)
			{
				CargarPerro();
			}
		else
			{
				Resultados();
			}
	}

function Resultados()
	{
		console.log("Los perros registrados son:")
		for(let perro of perrosRegistrados)
			{
				console.log(perro.nombre);
			}

		console.log("Los perros en adopción son:")
		for(let perro of perrosRegistrados)
			{
				if(perro.estado === estados[0])
					{
						console.log(perro.informar());		
					}
			}

		console.log("Los perros en proceso de adopción son:")
		for(let perro of perrosRegistrados)
			{
				if(perro.estado === estados[1])
					{
						console.log(perro.informar());		
					}
			}

		console.log("Los perros adoptados son:")
		for(let perro of perrosRegistrados)
			{
				if(perro.estado === estados[2])
					{
						console.log(perro.informar());		
					}
			}
	}

CargarPerro();